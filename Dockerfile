FROM python:3.10

# https://github.com/nodesource/distributions/blob/master/README.md#installation-instructions

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN apt-get update \
    && apt-get install --no-install-recommends -y \
     curl \
     gnupg \
    && curl -sL https://deb.nodesource.com/setup_20.x | bash - \
    && apt-get install --no-install-recommends  -y \
     nodejs \
    && npm install -g firebase-tools@13.*


RUN apt-get install apt-transport-https ca-certificates gnupg curl sudo -y
RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] http://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list && curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg  add - && apt-get update -y && apt-get install google-cloud-cli -y

RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*

COPY requirements.txt /usr/bin
WORKDIR /usr/bin
RUN pip install -r requirements.txt

COPY pipe /usr/bin/
COPY LICENSE.txt pipe.yml README.md /usr/bin/

ENTRYPOINT ["python3", "/usr/bin/main.py"]
