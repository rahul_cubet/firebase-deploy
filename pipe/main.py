import json
import logging
import os
import re
import subprocess
from io import StringIO

import yaml
from bitbucket_pipes_toolkit import Pipe, get_variable, get_logger

ansi_escape = re.compile(r'''
    \x1B  # ESC
    (?:   # 7-bit C1 Fe (except CSI)
        [@-Z\\-_]
    |     # or [ for CSI, followed by a control sequence
        \[
        [0-?]*  # Parameter bytes
        [ -/]*  # Intermediate bytes
        [@-~]   # Final byte
    )
''', re.VERBOSE)


def log_subprocess_output(pipe):
    for line in iter(pipe.readline, b''):  # b'\n'-separated lines
        logging.info(line.decode(errors='ignore'))


schema = {
    "FIREBASE_TOKEN": {"required": True, "type": "string"},
    "GCLOUD_CREDENTIALS": {"required": False, "type": "string"},
    "PROJECT_ID": {"required": False, "type": "string", "nullable": True, "default": None},
    "MESSAGE": {"type": "string", "required": False, "nullable": True, "default": None},
    "EXTRA_ARGS": {"type": "string", "required": False, "default": ''},
    "DEBUG": {"type": "boolean", "required": False, "default": False},
    "RETRY": {"type": "integer", "required": False, "default": 0},
}

logger = get_logger()


class FirebaseDeploy(Pipe):

    def run(self):
        logger.info(f">>>>>>>>>>>> START DEPLOY")
        fs = StringIO()
        logger.addHandler(logging.StreamHandler(fs))
        super().run()
        retry = self.get_variable('RETRY')
        logger.info(f'>>>>>>>>>>>> Executing the pipe... Retries: {retry}')
        project = self.get_variable('PROJECT_ID')
        token = self.get_variable('FIREBASE_TOKEN')
        message = self.get_variable('MESSAGE')
        extra_args = self.get_variable('EXTRA_ARGS')
        debug = self.get_variable('DEBUG')
        success = False
        if extra_args and "gcloud" in extra_args:
            logger.info(f">>>>>>>>>>>> GCLOUD DEPLOY")
            current_directory = os.getcwd()
            key = self.get_variable('GCLOUD_CREDENTIALS')
            logger.info(f">>>>>>>>>>>> project={project}")
            logger.info(f">>>>>>>>>>>> key={len(str(key))}")
            open("cred.json", 'w').write(key)
            logger.info(f">>>>>>>>>>>> gcloud auth")
            gcloud_command = [
                'gcloud',
                'auth',
                'activate-service-account',
                '--key-file=' + os.path.join(current_directory, 'cred.json')
            ]

            # Run the gcloud command to activate the service account
            try:
                subprocess.run(gcloud_command, check=True)
                logger.info(">>>>>>>>>>> Service account activated successfully.")
            except subprocess.CalledProcessError as e:
                logger.error(f">>>>>>>>>>> Service account activation failed with error code {e.returncode}.")

                self.fail(f'Failed to deploy project')
                exit(e.returncode)

            function_conf = json.load(open(os.path.join(current_directory, 'ci-functions.json')))

            vpc_functions = function_conf['vpc_conf']
            min_instsnce_functions = function_conf['min_instance_conf']

            logger.info(">>>>>>>>>>>>>\n"+str(function_conf)+">>>>>>>>>>>>>>")
            for function, connector in vpc_functions.items():
                logger.info(f">>>>>>>>>>>>>>> deploying {function} with {connector}")
                gcloud_command = [
                    'gcloud',
                    'functions',
                    'deploy',
                    function,
                    f'--vpc-connector={connector}',
                    f'--project={project}',
                    '--region=us-central1',
                    '--egress-settings=all',
                    '--no-gen2'
                ]

                process = subprocess.Popen(gcloud_command,
                                           stdout=subprocess.PIPE,
                                           stderr=subprocess.STDOUT,
                                           cwd=os.path.join(current_directory, 'functions')
                                           )
                with process.stdout:
                    log_subprocess_output(process.stdout)
                exitcode = process.wait()
                logging.info(f">>>>>>>>>>>>>> EXIT CODE: {exitcode}")
                if exitcode != 0:
                    logger.warning('>>>>>>>>>> Deployment failed.')
                    self.fail(f'Failed to deploy project')

            for function, c in min_instsnce_functions.items():
                logger.info(f">>>>>>>> deploying {function}, min instances: {c}")
                gcloud_command = [
                    'gcloud',
                    'functions',
                    'deploy',
                    function,
                    f'--min-instances={c}',
                    f'--project={project}',
                    '--region=us-central1','--no-gen2'
                ]

                process = subprocess.Popen(gcloud_command,
                                           stdout=subprocess.PIPE,
                                           stderr=subprocess.STDOUT,
                                           cwd=os.path.join(current_directory, 'functions')
                                           )
                with process.stdout:
                    log_subprocess_output(process.stdout)
                exitcode = process.wait()
                logging.info(f"EXIT CODE: {exitcode}")
                if exitcode != 0:
                    logger.warning('Deployment failed.')
                    self.fail(f'Failed to deploy project')

            self.success("Gcloud deploy complete")

        else:
            logger.info(f">>>>>>>>>>>> FIREBASE DEPLOY")
            commit = get_variable('BITBUCKET_COMMIT', default='local')
            repo = get_variable('BITBUCKET_REPO_SLUG', default='local')
            workspace = get_variable('BITBUCKET_WORKSPACE', default='local')

            if message is None:
                message = get_variable('MESSAGE',
                                       default=f'Deploy {commit} from https://bitbucket.org/{workspace}/{repo}')

            args = [
                'firebase',
                '--token', token,
                '--non-interactive'
            ]

            if debug:
                pass
                #args.append('--debug')

            if project is None:
                # get the project id from .firebaserc
                logger.info('Project id not specified, trying to fectch it from .firebaserc')
                try:
                    data = json.load(open('.firebaserc'))
                    project = data['projects']['default']
                except FileNotFoundError:
                    self.fail(message='.firebaserc file is missing and is required')
                except KeyError:
                    self.fail(message='Was not able to find project ID in .firebaserc. Check your configuration.')

            args.extend(['--project', project])
            args.extend(['deploy', '--message', message])

            args.extend(extra_args.split())

            logger.info('Starting deployment of the project to Firebase.')

            if extra_args and "storage" in extra_args:
                logger.info('Fetching storage indexes')
                with open("firestore.indexes.json", 'w') as f:
                    subprocess.call(["firebase", "firestore:indexes"], stdout=f)
            print(args)

            process = subprocess.Popen(args,
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.STDOUT
                                       )
            with process.stdout:
                log_subprocess_output(process.stdout)
            exitcode = process.wait()
            logging.info(f"EXIT CODE: {exitcode}")
            fs.seek(0)
            log = fs.read()
            logging.info(f"LOG ==> len={len(log)}, lines={len(log.split())}")
            fs.truncate(0)
            fs.seek(0)
            if exitcode != 0 or "Deploy complete!" not in log:
                logger.warning('Deployment failed.')
                # args_copy = args.copy()
                while retry > 0:
                    # logger.info(f'Retry {retry}')
                    # fs.seek(0)
                    # log = fs.read()
                    # fs.truncate(0)
                    # fs.seek(0)
                    # logger.warning(f"log length: {len(log)}")
                    # logger.info(f'Getting failed functions')
                    # logger.info(f"Log lines  " + str(log.split('\n')[-20:]))
                    #
                    # line = \
                    #     list(filter(lambda x: 'firebase deploy --only functions:' in ansi_escape.sub('', x),
                    #                 log.split('\n')))[
                    #         0]. \
                    #         replace('b\'', '').replace('\'', '').replace('\\n', '').strip()
                    # line = ansi_escape.sub('', line)
                    # logger.info(f'Line: {line}')
                    # args = args_copy.copy()
                    # args.extend(['--only', line.replace('firebase deploy --only ', '')])
                    logger.info(args)
                    process = subprocess.Popen(args,
                                               stdout=subprocess.PIPE,
                                               stderr=subprocess.STDOUT
                                               )
                    with process.stdout:
                        log_subprocess_output(process.stdout)
                    exitcode = process.wait()
                    logging.info(f"EXIT CODE: {exitcode}")
                    if exitcode != 0:
                        retry -= 1
                    else:
                        retry = 0
                        success = True
                        self.success(
                            f'Successfully deployed project {project}. Project link: https://console.firebase.google.com/project/{project}/overview')
            else:
                success = True
            if not success:
                self.fail(f'Failed to deploy project')


if __name__ == '__main__':
    with open('/usr/bin/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())
    pipe = FirebaseDeploy(pipe_metadata=metadata, schema=schema)
    pipe.run()
